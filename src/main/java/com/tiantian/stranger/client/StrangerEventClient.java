package com.tiantian.stranger.client;

import com.tiantian.stranger.settings.StrangerEventConfig;
import com.tiantian.stranger.thrift.event.StrangerEventService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 *
 */
public class StrangerEventClient {
    private TTransport transport;
    private StrangerEventService.Client client;

    public StrangerEventService.Client getClient() {
        return client;
    }

    public void setClient(StrangerEventService.Client client) {
        this.client = client;
    }

    public StrangerEventClient() {
        try {
            transport = new TSocket(StrangerEventConfig.getInstance().getHost(),
                    StrangerEventConfig.getInstance().getPort());
            transport.open();
            TFastFramedTransport fastTransport = new TFastFramedTransport(transport);

            TCompactProtocol protocol = new TCompactProtocol(fastTransport);

            client = new StrangerEventService.Client(protocol);
        } catch (TException x) {
            x.printStackTrace();
        }
    }

    public void close() {
        if (null != transport) {
            transport.close();
            client = null;
        }
    }
}
