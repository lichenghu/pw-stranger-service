package com.tiantian.stranger.client;

import com.tiantian.stranger.settings.StrangerEventConfig;
import com.tiantian.stranger.thrift.task.StrangerTaskService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 *
 */
public class StrangerTaskClient {
    private TTransport transport;
    private StrangerTaskService.Client client;

    public StrangerTaskService.Client getClient() {
        return client;
    }

    public void setClient(StrangerTaskService.Client client) {
        this.client = client;
    }

    public StrangerTaskClient() {
        try {
            transport = new TSocket(StrangerEventConfig.getInstance().getHost(),
                    StrangerEventConfig.getInstance().getPort());
            transport.open();
            TFastFramedTransport fastTransport = new TFastFramedTransport(transport);

            TCompactProtocol protocol = new TCompactProtocol(fastTransport);

            client = new StrangerTaskService.Client(protocol);
        } catch (TException x) {
            x.printStackTrace();
        }
    }

    public void close() {
        if (null != transport) {
            transport.close();
            client = null;
        }
    }
}
