package com.tiantian.stranger.utils;


import com.tiantian.stranger.client.StrangerEventFactory;
import com.tiantian.stranger.client.StrangerEventClient;
import com.tiantian.stranger.settings.StrangerEventConfig;
import com.tiantian.stranger.thrift.event.StrangerGroup;
import com.tiantian.stranger.thrift.event.StrangerGroupLogDetail;
import com.tiantian.stranger.thrift.event.StrangerResponse;
import com.tiantian.stranger.thrift.event.TableInfo;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class StrangerEventUtils {
    private ObjectPool<StrangerEventClient> pool;
    private StrangerEventUtils(){
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setTestOnBorrow(StrangerEventConfig.getInstance().isTestOnBorrow());
        config.setTestOnCreate(StrangerEventConfig.getInstance().isTestOnCreate());
        pool = new GenericObjectPool<StrangerEventClient>(new StrangerEventFactory(), config);
    }
    private static class SpingoEventUtilsHolder{
        private final static StrangerEventUtils instance = new StrangerEventUtils();
    }
    public static StrangerEventUtils getInstance(){
        return SpingoEventUtilsHolder.instance;
    }
    public String getServiceVersion(){
        StrangerEventClient client = null;
        String version = "";
        try {
            client = pool.borrowObject();
            version=client.getClient().getServiceVersion();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return version;
    }

    public void gameEvent(String cmd, String userId, String data, String tableId){
        StrangerEventClient client = null;
        try {
            client = pool.borrowObject();
            client.getClient().gameEvent(cmd, userId, data, tableId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Map<String, String> joinGame(String groupId, String userId) {
        StrangerEventClient client = null;
        Map<String, String> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().joinGame(groupId, userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean exitGame(String groupId, String userId) {
        StrangerEventClient client = null;
        boolean result;
        try {
            client = pool.borrowObject();
            result = client.getClient().exitGame(groupId, userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, String> sitDown(String groupId, String userId, int sitNum) {
        StrangerEventClient client = null;
        Map<String, String> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().sitDown(groupId, userId, sitNum);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean standUp(String groupId, String userId) {
        StrangerEventClient client = null;
        boolean result;
        try {
            client = pool.borrowObject();
            result = client.getClient().standUp(groupId, userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public TableInfo getTableInfo(String userId) {
        StrangerEventClient client = null;
        TableInfo result;
        try {
            client = pool.borrowObject();
            result = client.getClient().getTableInfo(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public StrangerResponse startGame(String groupId) {
        StrangerEventClient client = null;
        StrangerResponse result;
        try {
            client = pool.borrowObject();
            result = client.getClient().startGame(groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, String> getGroupUserStatus(String groupId) {
        StrangerEventClient client = null;
        Map<String, String> result;
        try {
            client = pool.borrowObject();
            result = client.getClient().getGroupUserStatus(groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, Long> applyBuyIn(String groupId, String userId, long buyIn) {
        StrangerEventClient client = null;
        Map<String, Long> result;
        try {
            client = pool.borrowObject();
            result = client.getClient().applyBuyIn(groupId, userId, buyIn);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, String> userHasJoinGroup(String userId, String groupId) {
        StrangerEventClient client = null;
        Map<String, String> result;
        try {
            client = pool.borrowObject();
            result = client.getClient().userHasJoinGroup(userId, groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, Long> getGameTimes(String groupId) {
        StrangerEventClient client = null;
        Map<String, Long> result;
        try {
            client = pool.borrowObject();
            result = client.getClient().getGameTimes(groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, Long> getBuyInInfo( String userId, String groupId) {
        StrangerEventClient client = null;
        Map<String, Long> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().getBuyInInfo(userId, groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public StrangerResponse agreeUserBuyIn(String groupId, String userId) {
        StrangerEventClient client = null;
        StrangerResponse result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().agreeUserBuyIn(groupId, userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public StrangerResponse changeSitDownType(String groupId, String userId, String type) {
        StrangerEventClient client = null;
        StrangerResponse result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().changeSitDownType(groupId, userId, type);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public StrangerResponse buySafe(String groupId, String userId, int buyIndex) {
        StrangerEventClient client = null;
        StrangerResponse result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().buySafe(groupId, userId, buyIndex);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, Long> getExchangeChipsInfo(String userId, String groupId) {
        StrangerEventClient client = null;
        Map<String, Long> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().getExchangeChipsInfo(userId, groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    public List<StrangerGroup> getUserReferGroups(String userId) {
        StrangerEventClient client = null;
        List<StrangerGroup> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().getUserReferGroups(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public StrangerGroupLogDetail getStrangerGroupDetail(String groupId) {
        StrangerEventClient client = null;
        StrangerGroupLogDetail result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().getStrangerGroupDetail(groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public StrangerResponse closeGame(String groupId) {
        StrangerEventClient client = null;
        StrangerResponse result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().closeGame(groupId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public StrangerResponse leaderAddUserDepositMoney(String groupId, String userId, int buyInCnt, long buyIn) {
        StrangerEventClient client = null;
        StrangerResponse result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().leaderAddUserDepositMoney(groupId, userId, buyInCnt, buyIn);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    public Map<String, Long> exChangeChips(String userId, String groupId, long buyInCnt) {
        StrangerEventClient client = null;
        Map<String, Long> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().exChangeChips(userId, groupId, buyInCnt);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, String> checkUserTableGroup(String userId) {
        StrangerEventClient client = null;
        Map<String, String> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().checkUserTableGroup(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
