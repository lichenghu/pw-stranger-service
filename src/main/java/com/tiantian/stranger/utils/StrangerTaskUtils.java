package com.tiantian.stranger.utils;

import com.tiantian.stranger.client.StrangerTaskClient;
import com.tiantian.stranger.client.StrangerTaskFactory;
import com.tiantian.stranger.settings.StrangerTaskConfig;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

/**
 *
 */
public class StrangerTaskUtils {
    private ObjectPool<StrangerTaskClient> pool;
    private StrangerTaskUtils(){
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setTestOnBorrow(StrangerTaskConfig.getInstance().isTestOnBorrow());
        config.setTestOnCreate(StrangerTaskConfig.getInstance().isTestOnCreate());
        pool = new GenericObjectPool<StrangerTaskClient>(new StrangerTaskFactory(), config);
    }
    private static class SpingoTaskUtilsHolder{
        private final static StrangerTaskUtils instance = new StrangerTaskUtils();
    }
    public static StrangerTaskUtils getInstance(){
        return SpingoTaskUtilsHolder.instance;
    }
    public String getServiceVersion(){
        StrangerTaskClient client = null;
        String version = "";
        try {
            client = pool.borrowObject();
            version=client.getClient().getServiceVersion();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return version;
    }

    public boolean execTask(String task){
        StrangerTaskClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().execTask(task);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null != client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
