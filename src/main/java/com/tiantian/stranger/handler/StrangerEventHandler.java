package com.tiantian.stranger.handler;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MapReduceIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.fc.thrift.core.StrangerWinLoseLog;
import com.tiantian.stranger.akka.ClusterClientManager;
import com.tiantian.stranger.akka.result.*;
import com.tiantian.stranger.akka.user.*;
import com.tiantian.stranger.conts.RedisConts;
import com.tiantian.stranger.data.mongodb.MGDatabase;
import com.tiantian.stranger.data.redis.RedisUtil;
import com.tiantian.stranger.entity.GroupEntity;
import com.tiantian.stranger.entity.GroupMemberEntity;
import com.tiantian.stranger.model.GameRecord;
import com.tiantian.stranger.thrift.event.*;
import com.tiantian.stranger.thrift.task.stranger_taskConstants;
import com.tiantian.stranger.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.printer.Printers;

import java.util.*;

/**
 *
 */
public class StrangerEventHandler implements StrangerEventService.Iface {
    static Logger LOG = LoggerFactory.getLogger(StrangerEventService.class);
    private static final String GROUPS_TABLE = "stranger_groups";
    private final static String CHECK_USER_IN_TABLE_SCRIPT_NAME_PATH = "redisscripts/check_user_in_table.lua";
    private static String checkUserInTableScriptSha = null;
    // 房间最大人数
    private static final int GROUP_MAX_USERS = 9;
    private static final int WAIT_SECS = 15;
    private static final long MAX_BUY_IN_CNT = 25; // 朋友场最大买入保证金
    private static final long MAX_EXCHANGE_BUY_IN_CNT = 15; // 朋友场最大买入保证金
    @Override
    public String getServiceVersion() throws TException {
        return stranger_taskConstants.version;
    }

    public void gameEvent(String cmd, String userId, String data, String tableId) throws TException {
        switch (cmd) {
            case "fold" :
                TableUserFoldEvent tableUserFoldEvent = new TableUserFoldEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserFoldEvent);
                break;
            case "check" :
                TableUserCheckEvent tableUserCheckEvent = new TableUserCheckEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserCheckEvent);
                break;
            case "call" :
                TableUserCallEvent tableUserCallEvent = new TableUserCallEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserCallEvent);
                break;
            case "raise" :
                try {
                    String[] datas = data.split(",");
                    long raise = Long.parseLong(datas[0]);
                    String pwd = datas[1];
                    TableUserRaiseEvent tableUserRaiseEvent = new TableUserRaiseEvent(tableId, userId, raise, pwd);
                    ClusterClientManager.send(tableUserRaiseEvent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case "allin" :
                TableUserAllinEvent tableUserAllinEvent = new TableUserAllinEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserAllinEvent);
                break;
            case "fast_raise" :
                try{
                    String[] datas = data.split(",");
                    TableUserFastRaiseEvent tableUserFastRaiseEvent = new TableUserFastRaiseEvent(tableId, userId, datas[0],  datas[1]);
                    ClusterClientManager.send(tableUserFastRaiseEvent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case "show_cards" :
                TableUserShowCardsEvent tableUserShowCardsEvent = new TableUserShowCardsEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserShowCardsEvent);
                break;
            case "add_time" : // 增加时间
                TableUserAddTimesEvent tableUserAddTimesEvent = new TableUserAddTimesEvent(tableId, userId);
                ClusterClientManager.send(tableUserAddTimesEvent);
                break;
            case "emoji" :
                try {
                    String[] datas = data.split(",");
                    String toUserId = datas[0];
                    String emoji = datas[1];
                    TableUserEmojiEvent tableUserEmojiEvent = new TableUserEmojiEvent(tableId, userId, toUserId, emoji);
                    ClusterClientManager.send(tableUserEmojiEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }

    }

    @Override
    public Map<String, String> joinGame(String groupId, String userId) throws TException {
        Map<String, String> map = new HashMap<>();
        GroupEntity group = getGroup(groupId);
        if (group == null || StringUtils.isBlank(group.getGroupId())) {
            map.put("status", "-1");
            map.put("errMsg", "局信息不存在");
            return map;
        }
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            map.put("status", "-2");
            map.put("errMsg", "该局已经结束");
            return map;
        }
        // 判断玩家是否已经在游戏里面
        long checkSitNum = checkUserInTable(groupId, userId);
        int sitNum = -1;
        if (checkSitNum > 0) { //已经存在房间里面
            sitNum = (int)checkSitNum;
        }
        else {
            // 检测玩家是否在其他房间里面
            String otherGroupId = RedisUtil.getFromMap(RedisConts.USER_GROUP_TABLE_KEY + userId, "tableId");
            if (StringUtils.isNotBlank(otherGroupId) && !otherGroupId.equalsIgnoreCase(groupId)) { // 还在其他房间里面
                exitGame(otherGroupId, userId); // 先退出
            }
        }

        List<GroupMemberEntity> groupMembers = group.getGroupMembers();
        boolean isInvite = false;
        if (groupMembers != null) {
            for (GroupMemberEntity groupMember : groupMembers) {
                String inviteUserId = groupMember.getUserId();
                if (userId.equalsIgnoreCase(inviteUserId)) {
                    isInvite = true;
                    break;
                }
            }
        }
        if(!isInvite) {
            // 自动加入到邀请列表中
            inviteMember(groupId, group.getMasterId(), userId);
            //初始化一条日志
            addUserWinLoseLog(group, userId);
        }
        String masterName = "";
        // UserDetail masterDetail = AccountUtils.getInstance().findUserDetailById(group.getMasterId());
        UserDetail masterDetail = AccountIface.instance().iface().findUserDetailByUserId(group.getMasterId());
        if (masterDetail != null && StringUtils.isNotBlank(masterDetail.getNickName())) {
            masterName = masterDetail.getNickName();
        }
       // UserDetail userDetail = AccountUtils.getInstance().findUserDetailById(userId);
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        // 发送异步加入游戏消息
        ClusterClientManager.send(new TableUserJoinEvent(groupId, userId,userDetail.getAvatarUrl(),
                userDetail.getNickName(), userDetail.getGender(), group.getBuyIn(), group.getBigBlind(),
                group.getSmallBlind(), sitNum, group.getMasterId(), group.isSafe(), (group.getStartData() + (long)(group.getHours() * 3600000l))));
        map.put("status", "0");
        map.put("errMsg", "");
        map.put("group_id", groupId);
        map.put("sit_num", sitNum + "");
        map.put("max_people", GROUP_MAX_USERS + "");
        map.put("smbm", group.getSmallBlind() + "");
        map.put("bmbm", group.getBigBlind() + "");
        map.put("buy_in", group.getBuyIn() + "");
        map.put("hours", group.getHours() >= 1 ? (group.getHours() + "").substring(0, 1) : (group.getHours() + "").substring(0, 3));
        map.put("master_name", masterName);
        map.put("is_master", group.getMasterId().equalsIgnoreCase(userId) ? "1" : "0");
        map.put("wait_secs", WAIT_SECS + "");
        return map;
    }

    @Override
    public boolean exitGame(String groupId, String userId) throws TException {
        Object ret = ClusterClientManager.sendAndWait(new TableUserExitEvent(userId, groupId, null, null), 20);
        return (boolean) ret;
    }

    @Override
    public Map<String, String> sitDown(String gId, String userId, int sitNum) throws TException {
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("group_id", "-1");
        resultMap.put("sit_num",  "0");
        resultMap.put("max_people",  "0");
        resultMap.put("smbm", "0");
        resultMap.put("bmbm", "0");
        resultMap.put("buy_in", "0");
        if (sitNum <= 0 || sitNum > 9) {
            resultMap.put("status", "-10");
            resultMap.put("errMsg", "座位号不正确");
            return resultMap;
        }
        String groupId = RedisUtil.getFromMap(RedisConts.USER_GROUP_TABLE_KEY + userId, "tableId");
        if (StringUtils.isBlank(groupId) ||  StringUtils.isBlank(gId) || !gId.equalsIgnoreCase(groupId)) {
            resultMap.put("status", "-11");
            resultMap.put("errMsg", "牌局已经关闭");
            return resultMap;
        }
        GroupEntity group = getGroup(groupId);
        if (group == null || StringUtils.isBlank(group.getGroupId())) {
            resultMap.put("status", "-1");
            resultMap.put("errMsg", "局信息不存在");
            return resultMap;
        }
        if(group.getForceOver() == 1) {
            resultMap.put("status", "-11");
            resultMap.put("errMsg", "牌局已经关闭");
            resultMap.put("group_id", groupId);
            return resultMap;
        }

        if((group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            resultMap.put("status", "-12");
            resultMap.put("errMsg", "牌局已结束，不能坐下");
            resultMap.put("group_id", groupId);
            return resultMap;
        }

        TableUserSitDownEvent.Response response = (TableUserSitDownEvent.Response) ClusterClientManager.sendAndWait(
                new TableUserSitDownEvent(groupId, userId, group.getBuyIn(), group.getMasterId(),
                group.getSmallBlind(), group.getBigBlind(), sitNum), 30);
        int status = response.getStatus();
        if (status < 0) {
            resultMap.put("status", status + "");   // -1  玩家还在桌子上不能做下, -2 // 座位已满, -3 //该座位号已经有人, -4 筹码不足
            if(status == -4) {
               // StrangerWinLoseLog strangerWinLoseLog = FriendCoreUtils.getInstance().getUserStrangerGroupWinLoseLog(groupId, userId);
                StrangerWinLoseLog strangerWinLoseLog = FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(groupId, userId);
                if (strangerWinLoseLog == null || strangerWinLoseLog.getMoneyLeft() <= 0
                        || strangerWinLoseLog.getMoneyLeft() < group.getBuyIn()) {
                    resultMap.put("status", "-5"); // 额度不足
                }
            }
            resultMap.put("errMsg", "");
            resultMap.put("group_id", groupId);
            return resultMap;
        }
        int userSitNum = response.getUserSitNum();
        resultMap.put("status", "0");
        resultMap.put("errMsg", "");
        resultMap.put("group_id", groupId);
        resultMap.put("sit_num", userSitNum + "");
        resultMap.put("max_people", GROUP_MAX_USERS + "");
        resultMap.put("smbm", group.getSmallBlind() + "");
        resultMap.put("bmbm", group.getBigBlind() + "");
        resultMap.put("buy_in", group.getBuyIn() + "");
        // 更新加入状态
        updateUserJoin(groupId, userId);
        return resultMap;
    }

    @Override
    public boolean standUp(String groupId, String userId) throws TException {
        return (boolean)ClusterClientManager.sendAndWait(new TableUserStandUpEvent(userId, groupId));
    }

    @Override
    public TableInfo getTableInfo(String userId) throws TException {
        String tableId = RedisUtil.getFromMap(RedisConts.USER_GROUP_TABLE_KEY + userId, "tableId");
        if (StringUtils.isBlank(tableId)) {
            return new TableInfo();
        }

        TableInfo gTableInfo = new TableInfo();
        TableInfoResult tableInfo =
                (TableInfoResult)ClusterClientManager.sendAndWait(new TableUserTableInfEvent(tableId, userId), 30);
        if (tableInfo != null) {
            gTableInfo.setTableId(tableId);
            gTableInfo.setInnerId(tableInfo.getInnerId());
            gTableInfo.setInnerCnt(tableInfo.getInnerCnt());
            List<UserInfo> userInfos = new ArrayList<>();
            if (tableInfo.getUsers() != null) {
                for (UserInfoResult userInfo : tableInfo.getUsers()) {
                    UserInfo $userInfo = new UserInfo();
                    $userInfo.setUserId(userInfo.getUserId());
                    $userInfo.setSitNum(userInfo.getSitNum());
                    $userInfo.setNickName(userInfo.getNickName());
                    $userInfo.setAvatarUrl(userInfo.getAvatarUrl());
                    $userInfo.setMoney(userInfo.getMoney());
                    $userInfo.setPlaying(userInfo.getPlaying());
                    $userInfo.setGender(userInfo.getGender());
                    userInfos.add($userInfo);
                }
            }
            gTableInfo.setUsers(userInfos);
            List<UserStatus> userStatuses = new ArrayList<>();
            if (tableInfo.getUserStatus() != null) {
                for (UserStatusResult userStatus : tableInfo.getUserStatus()) {
                    UserStatus $userStatus = new UserStatus();
                    $userStatus.setSn(userStatus.getSn());
                    $userStatus.setHandCards(userStatus.getHandCards());
                    $userStatus.setStatus(userStatus.getStatus());
                    userStatuses.add($userStatus);
                }
            }
            gTableInfo.setUserStatus(userStatuses);
            gTableInfo.setCurBetSit(tableInfo.getCurBetSit());
            gTableInfo.setDeskCards(tableInfo.getDeskCards());
            gTableInfo.setHandCards(tableInfo.getHandCards());
            gTableInfo.setCardLevel(tableInfo.getCardLevel());
            gTableInfo.setLeftSecs(tableInfo.getLeftSecs());
            gTableInfo.setPool(tableInfo.getPool());
            List<UserBet> userBets = new ArrayList<>();
            if (tableInfo.getUserBets() != null) {
                for (UserBetResult userBet : tableInfo.getUserBets()) {
                    UserBet $userBets = new UserBet();
                    $userBets.setSn(userBet.getSn());
                    $userBets.setBet(userBet.getBet());
                    userBets.add($userBets);
                }
            }
            gTableInfo.setUserBets(userBets);
            gTableInfo.setPwd(tableInfo.getPwd());
            gTableInfo.setUserCanOps(tableInfo.getUserCanOps());
            gTableInfo.setButton(tableInfo.getButton());
            gTableInfo.setSmallBtn(tableInfo.getSmallBtn());
            gTableInfo.setBigBtn(tableInfo.getBigBtn());
            gTableInfo.setSmallBtnMoney(tableInfo.getSmallBtnMoney());
            gTableInfo.setBigBtnMoney(tableInfo.getBigBtnMoney());
            gTableInfo.setShowBegin(tableInfo.getShowBegin());
            gTableInfo.setIsStopped(tableInfo.getIsStopped());
            gTableInfo.setGameStatus(tableInfo.getGameStatus());
            gTableInfo.setUserWait(tableInfo.getUserWaitStatus());
            gTableInfo.setSafeWaitSec(tableInfo.getSafeWaitSec());
            if (tableInfo.getUserSafes() != null && tableInfo.getUserSafes().size() > 0) {
                List<UserSafe> userSafes = Lists.newArrayList();
                for (UserSafeResult userSafeResult : tableInfo.getUserSafes()) {
                     UserSafe userSafe = new UserSafe(userSafeResult.getSitNum(), userSafeResult.getLeftSecs(),
                              userSafeResult.getPrice(), userSafeResult.getOuts(), userSafeResult.getLeftSafe());
                     userSafes.add(userSafe);
                }
                gTableInfo.setUserSafes(userSafes);
            }
        }
        return gTableInfo;
    }

    public StrangerResponse startGame(String groupId) {
        GroupEntity group = getGroup(groupId);
        long startData = group.getStartData();
        long endTimes = 0;
        long currentTimes = System.currentTimeMillis();
        if (currentTimes > startData) { // 应该已经开始了
            endTimes = startData + (long)(group.getHours() * 3600000.0d);
        }
        else { // 15分钟之内, 从当前时间开始计时
            endTimes = System.currentTimeMillis() + (long)(group.getHours() * 3600000.0d);
        }
        Object obj = ClusterClientManager.sendAndWait(new TableUserStartGameEvent(groupId, group.getBuyIn(), group.getSmallBlind(),
                group.getBigBlind(), endTimes, group.getMasterId(), group.getTotalPlayCnt()), 30);
        String result = (String) obj;

        if ("0".equalsIgnoreCase(result)) {
            //更新开始时间
            updateGroupStartDate(groupId);
            updateGroupStarted(groupId);
            return new StrangerResponse(0, "开始成功");
        }
        else if ("-1".equalsIgnoreCase(result)) {// 游戏已经开始过
            return new StrangerResponse(-2, "局人数不齐");
        }
        else if("-2".equalsIgnoreCase(result)){// 人数不齐
            return new StrangerResponse(-3, "局已经开开始");
        }
        return new StrangerResponse(-1, "局的信息不存在");
    }

    public StrangerResponse agreeUserBuyIn(String groupId, String userId) {
        GroupEntity group = getGroup(groupId);
        if (group == null) {
            return new StrangerResponse(-1, "局信息不存在");
        }
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return new StrangerResponse(-1, "局已经结束,操作失败");
        }
        // StrangerWinLoseLog strangerWinLoseLog = FriendCoreUtils.getInstance().getUserStrangerGroupWinLoseLog(groupId, userId);
        StrangerWinLoseLog strangerWinLoseLog = null;
        try {
            strangerWinLoseLog = FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(groupId, userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        long appBuyIn = strangerWinLoseLog.getAppMoneyBuyIn();
        if (appBuyIn <= 0) {
            return new StrangerResponse(-1, "买入信息不正确,无法增加保证金");
        }
        TableUserAgreeCreditMoneyEvent.Response response =
                (TableUserAgreeCreditMoneyEvent.Response) ClusterClientManager.sendAndWait(
                        new TableUserAgreeCreditMoneyEvent(groupId, userId), 30);
        return new StrangerResponse(response.getStatus(), response.getMessage());
    }

    public Map<String, String> getGroupUserStatus(String groupId) {
        return (Map<String, String>) ClusterClientManager.sendAndWait(new TableUserStatusEvent(groupId), 30);
    }

    public Map<String, Long> getBuyInInfo(String userId, String groupId) throws TException{
        Map<String, Long> map = new HashMap<>();
        GroupEntity group = getGroup(groupId);
        long buyIn = 0;
        if (group != null) {
            buyIn = group.getBuyIn();
        }
        map.put("cnt", MAX_BUY_IN_CNT);
        map.put("buy_in", buyIn);
        map.put("left_money", 0L);
        map.put("apply_money", 0L);
        // StrangerWinLoseLog winLoseLog =  FriendCoreUtils.getInstance().getUserStrangerGroupWinLoseLog(groupId, userId);
        StrangerWinLoseLog winLoseLog = FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(groupId, userId);
        if (winLoseLog == null) {
            map.put("cnt", 0L);
            return map;
        }
        map.put("apply_money", winLoseLog.getAppMoneyBuyIn());
        long moneyLeft = winLoseLog.getMoneyLeft();
        map.put("left_money", moneyLeft);
        if (group != null) {
            long cnt = MAX_BUY_IN_CNT;
            if (moneyLeft > 0) {
                cnt =  MAX_BUY_IN_CNT - (long) Math.ceil((double)Math.max(moneyLeft, buyIn)/(double)buyIn);
            }
            map.put("cnt", Math.max(0, cnt));
            map.put("buy_in", buyIn);
        }
        return map;
    }

    public Map<String, Long> getExchangeChipsInfo(String userId, String groupId) throws TException{
        Map<String, Long> map = new HashMap<>();
        GroupEntity group = getGroup(groupId);
        long buyIn = 0;
        if (group != null) {
            buyIn = group.getBuyIn();
        }
        map.put("cnt", MAX_EXCHANGE_BUY_IN_CNT);
        map.put("buy_in", buyIn);
        map.put("left_money", 0l);
        // StrangerWinLoseLog winLoseLog = FriendCoreUtils.getInstance().getUserStrangerGroupWinLoseLog(groupId, userId);
        StrangerWinLoseLog winLoseLog = FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(groupId, userId);
        if (winLoseLog != null) {
            map.put("left_money", winLoseLog.getMoneyLeft());
        }
        String userBuyInCnt = RedisUtil.getFromMap(RedisConts.USER_GROUP_TABLE_KEY + userId, "buyInCnt");
        long buyInCnt = 0;
        if (StringUtils.isNotBlank(userBuyInCnt)) {
            buyInCnt = Long.parseLong(userBuyInCnt);
        }
        String userChips = RedisUtil.getFromMap(RedisConts.USER_GROUP_GMAE_KEY + userId, groupId);
        JSONObject jsonObject = null;
        if (userChips != null) {
            jsonObject = com.alibaba.fastjson.JSON.parseObject(userChips);
            String chips = jsonObject.getString("chips");
            long userLeftChips = 0;
            if (StringUtils.isNotBlank(chips)) {
                userLeftChips = Long.parseLong(chips);
            }
            long leftChips = userLeftChips + buyInCnt * buyIn;
            long cnt = MAX_EXCHANGE_BUY_IN_CNT;
            if (leftChips > 0) {
                cnt =  MAX_EXCHANGE_BUY_IN_CNT - (long) Math.ceil((double)Math.max(leftChips, buyIn)/(double)buyIn);
            }
            map.put("cnt", Math.max(0, cnt));
            map.put("buy_in", buyIn);
        }
        return map;
    }

    public Map<String, Long> applyBuyIn(String groupId, String userId, long buyInCnt)  throws TException{ //申请买入
        Map<String, Long> retMap = new HashMap<>();
        retMap.put("cnt", 0l);
        retMap.put("buy_in", 0l);
        GroupEntity group = getGroup(groupId);
        if (group == null) {
            return retMap;
        }
        if (buyInCnt <= 0) {
            return retMap;
        }
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return retMap;
        }
        Map<String, Long> buyInInfoMap = getBuyInInfo(userId, groupId);
        long maxBuyInCnt = buyInInfoMap.get("cnt");
        if (buyInCnt > maxBuyInCnt) {
             // 买入数量超过了最大买入;
            retMap.put("cnt", -1l);
            return retMap;
        }

        TableUserApplyCreditEvent.Response response = (TableUserApplyCreditEvent.Response)
                  ClusterClientManager.sendAndWait(new TableUserApplyCreditEvent(groupId, userId,
                                                   buyInCnt * group.getBuyIn(), MAX_BUY_IN_CNT * group.getBuyIn(),
                                                   group.getMasterId()), 30);
        long cnt = buyInInfoMap.get("cnt");
        long buyIn = buyInInfoMap.get("buy_in");
        if (response.getStatus() == 0) {
            retMap.put("cnt", buyInCnt);
            retMap.put("buy_in", buyIn);
        } else {
            retMap.put("cnt", (long) response.getStatus());
            retMap.put("buy_in", 0L);
        }
        LOG.info("response, status {}, message", response.getStatus(), response.getMessage());
        return retMap;
    }

    public Map<String, Long> exChangeChips(String groupId, String userId, long buyInCnt) throws TException {
        Map<String, Long> retMap = new HashMap<>();
        retMap.put("cnt", 0l);
        retMap.put("buy_in", 0l);
        GroupEntity group = getGroup(groupId);
        if (group == null) {
            return retMap;
        }
        if (buyInCnt <= 0) {
            return retMap;
        }
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return retMap;
        }
        // 判断当前筹码
        Map<String, Long> buyInInfoMap = getExchangeChipsInfo(userId, groupId);
        long maxBuyInCnt = buyInInfoMap.get("cnt");
        if (buyInCnt > maxBuyInCnt) {
            // 买入数量超过了最大买入;
            return retMap;
        }

        TableUserExchangeChipsEvent.Response response = (TableUserExchangeChipsEvent.Response)
                ClusterClientManager.sendAndWait(new TableUserExchangeChipsEvent(userId, groupId,
                        buyInCnt, group.getBuyIn()), 30);

        long cnt = buyInInfoMap.get("cnt");
        long buyIn = buyInInfoMap.get("buy_in");
        if (response.getStatus() == 0) {
            retMap.put("cnt", buyInCnt);
            retMap.put("buy_in", buyIn);
        } else {
            retMap.put("cnt", (long) response.getStatus()); //-1, "保证金不足"
            retMap.put("buy_in", 0L);
        }
        LOG.info("response, status {}, message", response.getStatus(), response.getMessage());
        return retMap;

    }

    // 玩家是否还在房间里
    public Map<String, String> userHasJoinGroup(String userId, String groupId) throws TException {
        Map<String, String> map = new HashMap<>();
        GroupEntity group = getGroup(groupId);
        if(group != null && group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            map.put("status", "-2");
            map.put("errMsg", "该局已经结束");
            return map;
        }
        if (StringUtils.isNotBlank(groupId)) {
            LOG.info("has1 join group id is :" + groupId);
            String userTableKey = RedisConts.USER_GROUP_TABLE_KEY + userId;
            // Map<String, String> tableMap = RedisUtil.getMap(userTableKey);
            boolean isExists = RedisUtil.exists(userTableKey);
            LOG.info("has join isExists is :" + isExists + ",key" + userTableKey);
            //在游戏中
            if (isExists) {
                map.put("status", "0"); // 在房间
                map.put("errMsg", "");
                return map;
            }
        }
        LOG.info("has2 join group id is :" + groupId);
        map.put("status", "-1");
        map.put("errMsg", "已不在局中");
        return map;
    }

    public Map<String, Long> getGameTimes(String groupId) throws TException {
        Map<String, Long> map = new HashMap<>();

        GroupEntity group = getGroup(groupId);
        long startData = group.getStartData();
        long nowData = System.currentTimeMillis();
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            map.put("already_secs", (long) Math.min(group.getHours() * 3600.0d, Math.max(0.0d, (double)(nowData - startData) / 1000.0d)));
            map.put("left_secs", 0L);
            return map;
        }

        if (startData > nowData) { // 游戏未开始
            map.put("already_secs", 0L);
            map.put("left_secs", (long)(group.getHours() * 3600.0d));
        } else {
            long alreadySecs = (nowData - startData) / 1000; // 已经进行的时间
            long leftSecs = (long)(group.getHours() * 3600.0d) - alreadySecs; // 剩余的时间
            map.put("already_secs", alreadySecs);
            map.put("left_secs", leftSecs);
        }
        return map;
    }

    public StrangerResponse changeSitDownType(String groupId, String userId, String type) {
        TableUserChangeSitDowTypeEvent.Response response =
          (TableUserChangeSitDowTypeEvent.Response)
                  ClusterClientManager.sendAndWait(new TableUserChangeSitDowTypeEvent(groupId, userId, type), 30);
        return new  StrangerResponse(response.getStatus(), response.getMessage());
    }

    public StrangerResponse buySafe(String groupId, String userId, int buyIndex) {
        TableUserBuySafeEvent.Response response = (TableUserBuySafeEvent.Response)
                ClusterClientManager.sendAndWait(new TableUserBuySafeEvent(groupId, userId, buyIndex), 30);
        return new  StrangerResponse(response.getStatus(), response.getMessage());
    }

    public List<StrangerGroup> getUserReferGroups(String userId) throws TException {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        long currentMill = System.currentTimeMillis();
        String mapFunc = "function () {" +
                "if (!this || this == undefined) {return;}" +
                "var flag = (this.forceOver != 1 && (this.startDate == 0 || this.startDate + this.hours * 3600000 >= " + currentMill + "));" +
                "if (!flag) { " +
                "    return;" +
                "} "+
                "for (var val in this.groupMembers){" +
                "    if (this.groupMembers[val].userId == '" + userId + "') {" +
                "         emit(this._id, this);return; " +
                "}}} ";
        String reduceFunc = "function(key, values) {" +
                "              return values;" +
                "        }";

        MapReduceIterable<Document> out = groupsCollection.mapReduce(mapFunc, reduceFunc);
        MongoCursor<Document> cursor = out.iterator();
        Document document = null;
        List<StrangerGroup> list = new ArrayList<>();
        while (cursor.hasNext()) {
            document = cursor.next();
            ObjectId objectId = document.getObjectId("_id");
            Document doc =  (Document)document.get("value");
            String groupCode = doc.getString("groupCode");
            String masterId = doc.getString("masterId");
            String masterName = doc.getString("masterName");
            String masterAvatarUrl = doc.getString("masterAvatarUrl");
            long buyIn = doc.getLong("buyIn");
            double hours = doc.getDouble("hours");
            long startDate = doc.getLong("startDate");
            long fee = doc.getLong("fee");
            long createDate = doc.getLong("createDate");
            int forceOver = doc.getInteger("forceOver");
            long bigBlind =  doc.getLong("bigBlind");
            long smallBlind = doc.getLong("smallBlind");
            Long totalPlayCntObj = doc.getLong("totalPlayCnt");
            boolean safe = doc.getBoolean("safe");
            long totalPlayCnt = totalPlayCntObj == null ? 0 : totalPlayCntObj.longValue();
            List<Document> groupMembers = doc.get("groupMembers", List.class);
            List<StrangerGroupMember> groupMembersList = new ArrayList<>();
            if (groupMembers != null) {
                for (Document groupMemberDoc : groupMembers) {
                    String $userId = groupMemberDoc.getString("userId");
                    String nickName = groupMemberDoc.getString("nickName");
                    String mobile = groupMemberDoc.getString("mobile");
                    String avatarUrl = groupMemberDoc.getString("avatarUrl");
                    Integer isJoin = groupMemberDoc.getInteger("isJoin", 0);
                    StrangerGroupMember groupMember = new StrangerGroupMember($userId, mobile, nickName, avatarUrl, isJoin);
                    groupMembersList.add(groupMember);
                }
            }
            StrangerGroup group = new StrangerGroup(objectId.toString(), groupCode, masterId, masterName, masterAvatarUrl, buyIn, hours, fee, startDate,
                    createDate, forceOver, groupMembersList, bigBlind, smallBlind, totalPlayCnt, safe);
            list.add(group);
        }
        return list;
    }

    public StrangerGroupLogDetail getStrangerGroupDetail(String groupId) throws TException{
        StrangerGroupLogDetail groupLogDetail = new StrangerGroupLogDetail();
        GroupEntity group = getGroup(groupId);
        if (group == null) {
            return  groupLogDetail;
        }
        Map<String, Long> map = getGameTimes(groupId);
        groupLogDetail.setAlreadySecs(map.get("already_secs"));
        groupLogDetail.setLeftSecs(map.get("left_secs"));
        // List<StrangerWinLoseLog> winLoseLogs = FriendCoreUtils.getInstance().getStrangerGroupWinLoseLogs(groupId);
        List<StrangerWinLoseLog> winLoseLogs = FriendCoreIface.instance().iface().getStrangerGroupWinLoseLogs(groupId);
        List<StrangerGroupWinLoseLog> groupWinLoseLogs = new ArrayList<>();
        long createDate = 0;
        if (winLoseLogs != null && winLoseLogs.size() > 0) {
            for (StrangerWinLoseLog winLoseLog : winLoseLogs) {
                 if (winLoseLog.getMoneyBuyTotal() == 0) {
                     continue; // 过滤掉没有申请筹码的
                 }
                 StrangerGroupWinLoseLog groupWinLoseLog = new StrangerGroupWinLoseLog(winLoseLog.getLogId(),
                         winLoseLog.getGroupId(), winLoseLog.getMasterNickName(), winLoseLog.getUserId(),
                         winLoseLog.getNickName(), winLoseLog.getMobile(), winLoseLog.getWin(),
                         winLoseLog.getPlayCnt(), winLoseLog.getLeftChips(), winLoseLog.getChipsBuyTotal(),
                         winLoseLog.getMoneyBuyTotal(), winLoseLog.getMoneyLeft(), winLoseLog.getSafeWin(),
                         winLoseLog.getSafeCost(), winLoseLog.getCreateDate());
                if (createDate == 0) {
                    createDate = groupWinLoseLog.getCreateDate();
                }
                groupWinLoseLogs.add(groupWinLoseLog);
            }
        }
        groupLogDetail.setGroupWinLoseLogList(groupWinLoseLogs);
        groupLogDetail.setHours(group.getHours() >= 1 ? (group.getHours() + "").substring(0, 1) :
                (group.getHours() + "").substring(0, 3));
        groupLogDetail.setDate(DateFormatUtils.format(new Date(createDate), "yyyy-MM-dd HH:mm:ss"));
        groupLogDetail.setTotalPlayCnt(group.getTotalPlayCnt());
        return groupLogDetail;
    }

    public StrangerResponse closeGame(String groupId) {
        TableUserCloseGameEvent.Response response =
                (TableUserCloseGameEvent.Response) ClusterClientManager.sendAndWait(new TableUserCloseGameEvent(groupId), 30);
        if (response.getStatus() == 0) {
            forceOverGroup(groupId);
        }
        return new StrangerResponse(response.getStatus(), response.getMessage());
    }

    public StrangerResponse leaderAddUserDepositMoney(String groupId, String userId, int buyInCnt, long buyIn) throws TException{
        GroupEntity group = getGroup(groupId);
        if (group == null) {
            return new StrangerResponse(-1, "牌局信息不存在");
        }

        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return new StrangerResponse(-2, "牌局已经结束不能增加筹码");
        }

        Map<String, Long> buyInInfoMap = getBuyInInfo(userId, groupId);
        long maxBuyInCnt = buyInInfoMap.get("cnt");
        if (buyInCnt > maxBuyInCnt) {
            // 买入数量超过了最大买入;
            return new StrangerResponse(-1, "超过玩最大带入补充筹码失败");
        }

        LeaderAddMoneyEvent.Response response =
                (LeaderAddMoneyEvent.Response) ClusterClientManager.sendAndWait(new LeaderAddMoneyEvent(groupId, buyInCnt, buyIn, userId), 30);
        return new StrangerResponse(response.getStatus(), response.getMessage());
    }

    public Map<String, String> checkUserTableGroup(String userId) throws TException {
        String userTableKey = RedisConts.USER_GROUP_TABLE_KEY + userId;
        Map<String, String> map = RedisUtil.getMap(userTableKey);
        if (map != null) {
            String tableId = map.get("tableId");
            if (StringUtils.isNotBlank(tableId)) {
                String tableStatusKey = RedisConts.GROUP_TABLE_STATUS_KEY + tableId;
                boolean tableExists = RedisUtil.exists(tableStatusKey);
                String userOnlineKey = RedisConts.GROUP_TABLE_USER_ONLINE_KEY + tableId;
                boolean userOnlineExists = RedisUtil.hexists(userOnlineKey, userId);
                if (tableExists && userOnlineExists) {
                    GroupEntity group = getGroup(tableId);
                    if (group == null) {
                        return new HashMap<>();
                    }
                    // 产生事件重新进入房间
                    return joinGame(tableId, userId);
                }
            }
        }
        return new HashMap<>();
    }

    @Override
    public String getGameRecord(String tableId, String userId, int cnt) throws TException {
        if (tableId != null) {
            if (cnt <= 0) {
                cnt = 1;
            }
            GameRecord gameRecord = RecordUtils.getRecordByIndex(tableId, cnt);
            if (gameRecord == null) {
                return "";
            }
            gameRecord.generateProgresses();
            return Printers.printString(Printers.prettyPrinterProtocol(), gameRecord.getRecord());
        }
        return "";
    }

    private boolean updateGroupStartDate(String groupId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        updateCondition.put("startDate",  new BasicDBObject("$gt", System.currentTimeMillis()));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("startDate", System.currentTimeMillis());
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    private boolean updateGroupStarted(String groupId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("started", "1");
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    private GroupEntity getGroup(String groupId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        FindIterable<Document> limitIter = groupsCollection.find(new BasicDBObject("_id", new ObjectId(groupId)))
                .sort(new BasicDBObject()).limit(1);
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            ObjectId objectId = doc.getObjectId("_id");
            String groupCode = doc.getString("groupCode");
            String masterId = doc.getString("masterId");
            String masterName = doc.getString("masterName");
            String masterAvatarUrl = doc.getString("masterAvatarUrl");
            long buyIn = doc.getLong("buyIn");
            double hours = doc.getDouble("hours");
            long startDate = doc.getLong("startDate");
            long fee = doc.getLong("fee");
            long createDate = doc.getLong("createDate");
            int forceOver = doc.getInteger("forceOver");
            long bigBlind =  doc.getLong("bigBlind");
            long smallBlind = doc.getLong("smallBlind");
            Long totalPlayCntObj = doc.getLong("totalPlayCnt");
            boolean safe = doc.getBoolean("safe");
            long totalPlayCnt = totalPlayCntObj == null ? 0 : totalPlayCntObj.longValue();
            List<Document> groupMembers = doc.get("groupMembers", List.class);
            List<GroupMemberEntity> groupMembersList = new ArrayList<>();
            if (groupMembers != null) {
                for (Document groupMemberDoc : groupMembers) {
                    String userId = groupMemberDoc.getString("userId");
                    String nickName = groupMemberDoc.getString("nickName");
                    String mobile = groupMemberDoc.getString("mobile");
                    String avatarUrl = groupMemberDoc.getString("avatarUrl");
                    Integer isJoin = groupMemberDoc.getInteger("isJoin", 0);
                    GroupMemberEntity groupMember = new GroupMemberEntity(userId, mobile, nickName, avatarUrl, isJoin);
                    groupMembersList.add(groupMember);
                }
            }
            return new GroupEntity(objectId.toString(), groupCode, masterId, masterName, masterAvatarUrl, buyIn, hours, fee, startDate,
                    createDate, forceOver, groupMembersList, bigBlind, smallBlind, totalPlayCnt, safe);
        }
        return new GroupEntity();
    }

    public long checkUserInTable(String groupId, String userId) {
        if (checkUserInTableScriptSha == null) {
            checkUserInTableScriptSha = RedisUtil.loadScript(CHECK_USER_IN_TABLE_SCRIPT_NAME_PATH);
        }
        List<String> keys = new ArrayList<>();
        keys.add(RedisConts.GROUP_TABLE_USER_KEY);
        keys.add(groupId);
        keys.add(userId);
        keys.add(RedisConts.USER_GROUP_TABLE_KEY);
        // 不需要vals 直接使用keys代替
        Object result = RedisUtil.execScript(checkUserInTableScriptSha, keys, keys);
        return (long) result;
    }

    // 邀请某人入局
    public GroupEntity inviteMember(String groupId,String masterUserId, String userId) throws TException {
        // 添加成员
        // UserDetail userDetail = AccountUtils.getInstance().findUserDetailById(userId);
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        if (userDetail != null && StringUtils.isBlank(userDetail.getUserId())) {
            LOG.info("user not exists user id :" + userId);
            //局成员信息不存在
            return new GroupEntity();
        }
        GroupEntity group = getGroup(groupId);
        if (group == null || StringUtils.isBlank(group.getGroupId())) {
            LOG.info("group not exists group id :" + groupId);
            return new GroupEntity();
        }
        // 不是房主
        if (!group.getMasterId().equalsIgnoreCase(masterUserId)) {
            return new GroupEntity();
        }
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return new GroupEntity();
        }
        List<GroupMemberEntity> groupMemberList = group.getGroupMembers();
        for (GroupMemberEntity groupMember : groupMemberList) {
            if(groupMember.getUserId().equalsIgnoreCase(userId)) { // 玩家以存在
                return new GroupEntity();
            }
        }
        // 添加成员
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        FindIterable<Document> limitIter = groupsCollection.find(new BasicDBObject("_id", new ObjectId(groupId)))
                .sort(new BasicDBObject()).limit(1);
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        List<Document> groupMembers = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            groupMembers = doc.get("groupMembers", List.class);
            if (groupMembers == null) {
                groupMembers = new ArrayList<>();
            }
            Document memDocument = new Document();
            memDocument.put("userId", userId);
            memDocument.put("isMaster", 0);
            memDocument.put("isJoin", 0);
            memDocument.put("nickName", userDetail.getNickName());
            memDocument.put("avatarUrl", userDetail.getAvatarUrl());
            memDocument.put("createDate", System.currentTimeMillis());
            groupMembers.add(memDocument);
        }
        if (groupMembers == null) {
            return new GroupEntity();
        }
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("groupMembers", groupMembers);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        groupsCollection.updateOne(updateCondition, updateSetValue);
        return new GroupEntity();
    }

    private void addUserWinLoseLog(GroupEntity group, String userId) {
        String masterNickName = group.getMasterName();
        String nickName = "";
      //  UserDetail userDetail = AccountUtils.getInstance().findUserDetailById(userId);
        UserDetail userDetail = null;
        try {
            userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        String mobile = "";
        if (userDetail != null) {
            nickName = userDetail.getNickName();
            mobile = userDetail.getMobile();
        }

        // 添加一条日志
//        FriendCoreUtils.getInstance().createStrangerWinLoseLogs(group.getGroupId(),
//                masterNickName, userId, nickName, group.getBuyIn(), mobile);
        try {
            FriendCoreIface.instance().iface().createStrangerWinLoseLogs(group.getGroupId(),
                    masterNickName, userId, nickName, group.getBuyIn(), mobile);
        } catch (TException e) {
            e.printStackTrace();
        }
    }

    private boolean updateUserJoin(String groupId, String userId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        updateCondition.put("groupMembers.userId", userId);
        updateCondition.put("groupMembers.isJoin", 0);

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("groupMembers.$.isJoin", 1);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return result.getModifiedCount() > 0;
    }

    private boolean forceOverGroup(String groupId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("forceOver", 1);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

}
