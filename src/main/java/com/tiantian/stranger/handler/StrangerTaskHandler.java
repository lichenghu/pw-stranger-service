package com.tiantian.stranger.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.ClusterClientManager;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.stranger.conts.GameConstants;
import com.tiantian.stranger.thrift.task.StrangerTaskService;
import com.tiantian.stranger.thrift.task.stranger_taskConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

/**
 *
 */
public class StrangerTaskHandler implements StrangerTaskService.Iface{
    @Override
    public String getServiceVersion() throws TException {
        return stranger_taskConstants.version;
    }

    @Override
    public boolean execTask(String taskStr) throws TException {
        try {
            JSONObject jsonObject = JSON.parseObject(taskStr);
            String taskEvent = jsonObject.getString(GameConstants.TASK_EVENT);
            if (StringUtils.isNotBlank(taskEvent)) {
                TableTaskEvent event = new TableTaskEvent();
                event.setEvent(taskEvent);
                event.setParams(jsonObject);
                ClusterClientManager.send(event);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
