package com.tiantian.stranger.entity;

import java.util.List;

/**
 *
 */
public class GroupEntity {
    private String groupId;
    private String groupCode;
    private String masterId;
    private String masterName;
    private String masterAvatarUrl;
    private long buyIn;
    private double hours;
    private long fee;
    private long startData;
    private long createDate;
    private int forceOver;
    private List<GroupMemberEntity> groupMembers;
    private long bigBlind;
    private long smallBlind;
    private long totalPlayCnt;
    private boolean safe; // 开启保险 1是 0否
    public GroupEntity() {
    }

    public GroupEntity(String groupId, String groupCode, String masterId, String masterName, String masterAvatarUrl,
                       long buyIn, double hours, long fee, long startData,
                       long createDate, int forceOver, List<GroupMemberEntity> groupMembers, long bigBlind,
                       long smallBlind, long totalPlayCnt, boolean safe) {
        this.groupId = groupId;
        this.groupCode = groupCode;
        this.masterId = masterId;
        this.masterName = masterName;
        this.masterAvatarUrl = masterAvatarUrl;
        this.buyIn = buyIn;
        this.hours = hours;
        this.fee = fee;
        this.startData = startData;
        this.createDate = createDate;
        this.forceOver = forceOver;
        this.groupMembers = groupMembers;
        this.bigBlind = bigBlind;
        this.smallBlind = smallBlind;
        this.totalPlayCnt = totalPlayCnt;
        this.safe = safe;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(long buyIn) {
        this.buyIn = buyIn;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }

    public long getStartData() {
        return startData;
    }

    public void setStartData(long startData) {
        this.startData = startData;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getForceOver() {
        return forceOver;
    }

    public void setForceOver(int forceOver) {
        this.forceOver = forceOver;
    }

    public List<GroupMemberEntity> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMemberEntity> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(long bigBlind) {
        this.bigBlind = bigBlind;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(long smallBlind) {
        this.smallBlind = smallBlind;
    }

    public long getTotalPlayCnt() {
        return totalPlayCnt;
    }

    public void setTotalPlayCnt(long totalPlayCnt) {
        this.totalPlayCnt = totalPlayCnt;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getMasterAvatarUrl() {
        return masterAvatarUrl;
    }

    public void setMasterAvatarUrl(String masterAvatarUrl) {
        this.masterAvatarUrl = masterAvatarUrl;
    }

    public boolean isSafe() {
        return safe;
    }

    public void setSafe(boolean safe) {
        this.safe = safe;
    }
}
