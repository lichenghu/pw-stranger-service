package com.tiantian.stranger.entity;

/**
 *
 */
public class GroupMemberEntity {
    private String userId;
    private String mobile;
    private String nickName;
    private String avatarUrl;
    private int isJoin;

    public GroupMemberEntity() {

    }
    public GroupMemberEntity(String userId, String mobile, String nickName, String avatarUrl, int isJoin) {
        this.userId = userId;
        this.mobile = mobile;
        this.nickName = nickName;
        this.avatarUrl = avatarUrl;
        this.isJoin = isJoin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getIsJoin() {
        return isJoin;
    }

    public void setIsJoin(int isJoin) {
        this.isJoin = isJoin;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
