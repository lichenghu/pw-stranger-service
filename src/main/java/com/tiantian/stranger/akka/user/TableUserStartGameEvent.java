package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserStartGameEvent extends TableUserEvent {

    private String groupId;
    private long buyIn;
    private long smallBlind;
    private long bigBlind;
    private long endTimes;
    private String masterId;
    private long maxUsers;

    public TableUserStartGameEvent(String groupId, long buyIn, long smallBlind, long bigBlind,
                                   long endTimes, String masterId, long maxUsers) {
        super(true);
        this.groupId = groupId;
        this.buyIn = buyIn;
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        this.endTimes = endTimes;
        this.masterId = masterId;
        this.maxUsers = maxUsers;
    }

    @Override
    public String tableId() {
        return groupId;
    }

    @Override
    public String event() {
        return "startGame";
    }

    public String getGroupId() {
        return groupId;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public long getEndTimes() {
        return endTimes;
    }

    public String getMasterId() {
        return masterId;
    }

    public long getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(long maxUsers) {
        this.maxUsers = maxUsers;
    }
}
