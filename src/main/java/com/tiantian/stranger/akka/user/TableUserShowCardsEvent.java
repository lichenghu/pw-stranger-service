package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserShowCardsEvent extends TableUserEvent {
    private String groupId;
    private String userId;
    private String data;

    public TableUserShowCardsEvent(String groupId, String userId, String data) {
        this.groupId = groupId;
        this.userId = userId;
        this.data = data;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String tableId() {
        return groupId;
    }

    @Override
    public String event() {
        return "showCards";
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
