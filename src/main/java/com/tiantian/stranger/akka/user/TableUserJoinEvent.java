package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserJoinEvent extends TableUserEvent {
    private String groupId;
    private String userId;
    private String avatarUrl;
    private String nickName;
    private String gender;
    private long buyIn;
    private long bigBlind;
    private long smallBlind;
    private int sitNum;
    private String masterId;
    private boolean safe;
    private long endDate;

    public TableUserJoinEvent(String groupId, String userId, String avatarUrl, String nickName, String gender, long buyIn, long bigBlind, long smallBlind, int sitNum,
                              String masterId, boolean safe, long endDate) {
        this.groupId = groupId;
        this.userId = userId;
        this.avatarUrl = avatarUrl;
        this.nickName = nickName;
        this.gender = gender;
        this.buyIn = buyIn;
        this.bigBlind = bigBlind;
        this.smallBlind = smallBlind;
        this.sitNum = sitNum;
        this.masterId = masterId;
        this.safe = safe;
        this.endDate = endDate;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUserId() {
        return userId;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public int getSitNum() {
        return sitNum;
    }

    public String getMasterId() {
        return masterId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String tableId() {
        return groupId;
    }

    @Override
    public String event() {
        return "userJoin";
    }

    public boolean isSafe() {
        return safe;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
}
