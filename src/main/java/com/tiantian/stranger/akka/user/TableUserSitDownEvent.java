package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

import java.io.Serializable;

/**
 *
 */
public class TableUserSitDownEvent extends TableUserEvent {
    private String groupId;
    private String userId;
    private long buyIn;
    private String masterId;
    private long smallBlind;
    private long bigBlind;
    private int userSitNum;
    private long totalBuyIn; // 用来判断买入总量 提示所有人带入信息

    public TableUserSitDownEvent(String groupId, String userId, long buyIn, String masterId, long smallBlind,
                                 long bigBlind, int userSitNum) {
        super(true);
        this.groupId = groupId;
        this.userId = userId;
        this.buyIn = buyIn;
        this.masterId = masterId;
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        this.userSitNum = userSitNum;
    }

    @Override
    public String tableId() {
        return groupId;
    }

    @Override
    public String event() {
        return "userSitDown";
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUserId() {
        return userId;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public String getMasterId() {
        return masterId;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public int getUserSitNum() {
        return userSitNum;
    }

    public long getTotalBuyIn() {
        return totalBuyIn;
    }

    public void setTotalBuyIn(long totalBuyIn) {
        this.totalBuyIn = totalBuyIn;
    }

    public static class Response implements Serializable {
        private int status;
        private String message;
        private int userSitNum;

        public Response(int status, String message, int userSitNum) {
            this.status = status;
            this.message = message;
            this.userSitNum = userSitNum;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getUserSitNum() {
            return userSitNum;
        }

        public void setUserSitNum(int userSitNum) {
            this.userSitNum = userSitNum;
        }
    }
}
