package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserExitEvent extends TableUserEvent {
    private String userId;
    private String tableId;
    private String sitNum;
    private String nickName;

    public TableUserExitEvent(String userId, String tableId, String sitNum, String nickName) {
        super(true);
        this.userId = userId;
        this.tableId = tableId;
        this.sitNum = sitNum;
        this.nickName = nickName;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userExit";
    }

    public String getUserId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }

    public String getSitNum() {
        return sitNum;
    }

    public String getNickName() {
        return nickName;
    }
}
