package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

import java.io.Serializable;

/**
 *
 */
public class TableUserChangeSitDowTypeEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private String type;

    public TableUserChangeSitDowTypeEvent(String tableId, String userId, String type) {
        super(true);
        this.tableId = tableId;
        this.userId = userId;
        this.type = type;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userChangeSitDownType";
    }

    public String getUserId() {
        return userId;
    }

    public String getType() {
        return type;
    }

    public static class Response implements Serializable {
        private int status;
        private String message;

        public Response(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
