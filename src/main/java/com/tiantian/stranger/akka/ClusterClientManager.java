package com.tiantian.stranger.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.cluster.client.ClusterClient;
import akka.cluster.client.ClusterClientSettings;
import akka.pattern.Patterns;
import akka.serialization.Serialization;
import akka.serialization.SerializationExtension;
import akka.serialization.Serializer;
import akka.util.Timeout;
import com.tiantian.stranger.akka.event.TableEvent;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.concurrent.TimeUnit;

/**
 *
 */
public class ClusterClientManager {
    private static ActorSystem actorSystem;
    private static ActorRef client;
    private static final String SEND_ACTOR_PATH = "/user/ShardingManager";
    private static Serializer serializer;
    public static void init (String port) {
        Config config = ConfigFactory.parseString(
                "akka.remote.netty.tcp.port=" + port).withFallback(
                ConfigFactory.load("clusterconfig.conf"));

        actorSystem = ActorSystem.create("StrangerActorSystem", config);
        client = actorSystem.actorOf(ClusterClient.props(
                        ClusterClientSettings.create(actorSystem)),
                "client");

        // Get the Serialization Extension
        Serialization serialization = SerializationExtension.get(actorSystem);
        serializer = serialization.findSerializerFor("msgpack");
    }

    private static void checkInit() {
        if (actorSystem == null) {
            throw new RuntimeException("actorSystem not init");
        }
        if (client == null) {
            throw new RuntimeException("client not init");
        }
    }

    public static void send(TableEvent event) {
        checkInit();
//        String tbId = event.tableId();
//        int hCode = Math.abs(tbId.hashCode());
//        byte[] heads = intToBytes(hCode);
//        byte[] bodys = serializer.toBinary(event);
//        int len = heads.length + bodys.length;
//        byte[] data = new byte[len];
//        System.arraycopy(heads, 0, data, 0, heads.length);
//        System.arraycopy(bodys, 0, data, heads.length, bodys.length);
        client.tell(new ClusterClient.Send(SEND_ACTOR_PATH,  event, true),
                ActorRef.noSender());
    }

    public static void sendToAll(Object object) {
        checkInit();
        client.tell(new ClusterClient.SendToAll(SEND_ACTOR_PATH,  object),
                ActorRef.noSender());
    }

    public static Object sendAndWait(Object object) {
        return sendAndWait(object, 30);
    }

    public static Object sendAndWait(Object object, long waitSec) {
        checkInit();
        Timeout timeout = Timeout.apply(Math.max(1, waitSec), TimeUnit.SECONDS);
        Future<Object> future = Patterns.ask(client,
                new ClusterClient.Send(SEND_ACTOR_PATH, object), timeout);
        Object result = null;
        try {
            result = Await.result(future, timeout.duration());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static byte[] intToBytes(int n){
        byte[] b = new byte[4];
        for(int i = 0;i < 4;i++)
        {
            b[i]=(byte)(n>>(24-i*8));
        }
        return b;
    }


}
