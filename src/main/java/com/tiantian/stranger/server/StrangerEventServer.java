package com.tiantian.stranger.server;

import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.ClusterClientManager;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.stranger.akka.user.TableUserStatusEvent;
import com.tiantian.stranger.data.mongodb.MGDatabase;
import com.tiantian.stranger.data.postgresql.PGDatabase;
import com.tiantian.stranger.handler.StrangerEventHandler;
import com.tiantian.stranger.settings.PGConfig;
import com.tiantian.stranger.settings.StrangerEventConfig;
import com.tiantian.stranger.thrift.event.StrangerEventService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 *
 */
public class StrangerEventServer {
    private static Logger LOG = LoggerFactory.getLogger(StrangerEventServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;


    public static void main(String[] args) {
        ClusterClientManager.init((StrangerEventConfig.getInstance().getPort() + 5) + "");
//        new Thread(() -> {
//            try {
//                TimeUnit.SECONDS.sleep(3);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            for (int i = 0; i < 5000; i++) {
//                ClusterClientManager.send(new TableUserStatusEvent(i+""));
//            }
//        }).start();
        StrangerEventServer strangerEventServer = new StrangerEventServer();
        strangerEventServer.init(args);
        strangerEventServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            StrangerEventHandler handler = new StrangerEventHandler();

            TProcessor tProcessor = new StrangerEventService.Processor<StrangerEventService.Iface>(handler);
            TServerSocket tss = new TServerSocket(StrangerEventConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());

        }
        catch (Exception e) {
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", StrangerEventConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
