package com.tiantian.stranger.server;

import com.tiantian.stranger.akka.ClusterClientManager;
import com.tiantian.stranger.data.mongodb.MGDatabase;
import com.tiantian.stranger.data.postgresql.PGDatabase;
import com.tiantian.stranger.handler.StrangerTaskHandler;
import com.tiantian.stranger.settings.PGConfig;
import com.tiantian.stranger.settings.StrangerTaskConfig;
import com.tiantian.stranger.thrift.task.StrangerTaskService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class StrangerTaskServer {
    private static Logger LOG = LoggerFactory.getLogger(StrangerTaskServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {

        ClusterClientManager.init((StrangerTaskConfig.getInstance().getPort() + 5) + "");

        StrangerTaskServer strangerTaskServer = new StrangerTaskServer();
        strangerTaskServer.init(args);
        strangerTaskServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            StrangerTaskHandler handler = new StrangerTaskHandler();
            TProcessor tProcessor = new StrangerTaskService.Processor<StrangerTaskService.Iface>(handler);
            TServerSocket tss = new TServerSocket(StrangerTaskConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());
        }
        catch (Exception e) {
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", StrangerTaskConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
