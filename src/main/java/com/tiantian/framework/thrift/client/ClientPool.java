package com.tiantian.framework.thrift.client;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class ClientPool {
    private GenericObjectPool<TProtocol> protocolPool;

    public TProtocol borrowObject() throws Exception {
        return protocolPool.borrowObject();
    }

    public void returnObject(TProtocol protocol) {
        protocolPool.returnObject(protocol);
    }

    public ClientPool(String host, int port, GenericObjectPoolConfig poolConfig) {
        if (poolConfig == null) {
            poolConfig = new GenericObjectPoolConfig();
        }
        // 初始化对象池
        protocolPool = new GenericObjectPool<>(new ClientFactory(host, port), poolConfig);
    }

    public ClientPool(String host, int port) {
        this(host, port, null);
    }
}
