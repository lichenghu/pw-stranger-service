namespace java com.tiantian.stranger.thrift.event

const string version = '1.0.0'

struct StrangerResponse {
  1:i32 status,
  2:string message
}

struct UserStatus {
 1:i32 sn,
 2:string status,
 3:string handCards
}

struct UserBet {
 1:i32 sn,
 2:i64 bet
}

struct UserInfo {
  1:string userId,
  2:i32 sitNum,
  3:string  avatarUrl,
  4:string  nickName,
  5:i64  money,
  6:i32  playing,
  7:string gender
}

struct UserSafe {
  1:i32 sitNum
  2:i64 leftSecs
  3:list<string> price
  4:list<string> outs
  5:i64 leftSafe
}

struct TableInfo {
    1:list<UserInfo>    users,
    2:list<UserStatus>  userStatus,
    3:string            deskCards,
    4:list<UserBet>     userBets,
    5:string            innerId,
    6:i32               curBetSit,
    7:string            handCards,
    8:string            pool,
    9:i64               leftSecs,
    10:string           innerCnt,
    11:string           userCanOps,
    12:i32              button,
    13:i32              smallBtn,
    14:i32              bigBtn,
    15:i32              smallBtnMoney,
    16:i32              bigBtnMoney,
    17:i32              showBegin,
    18:i32              isStopped,
    19:string           gameStatus,
    20:string           pwd,
    21:string           cardLevel,
    22:string           tableId
    23:string           userWait
    24:list<UserSafe>   userSafes
    25:i64              safeWaitSec
}

struct StrangerGroupMember {
     1:string userId,
     2:string mobile,
     3:string nickName,
     4:string avatarUrl,
     5:i64 isJoin
}

struct StrangerGroup {
    1:string groupId,
    2:string groupCode,
    3:string masterId,
    4:string masterName,
    5:string masterAvatarUrl,
    6:i64 buyIn,
    7:double hours,
    8:i64 fee,
    9:i64 startData,
    10:i64 createDate,
    11:i32 forceOver,
    12:list<StrangerGroupMember> groupMembers,
    13:i64 bigBlind,
    14:i64 smallBlind,
    15:i64 totalPlayCnt,
    16:bool safe
}

struct StrangerGroupWinLoseLog {
   1:string logId,
   2:string groupId,
   3:string masterNickName,
   4:string userId,
   5:string nickName,
   6:string mobile,
   7:i64  win,
   8:i32  playCnt,
   9:i64  leftChips,
   10:i64 chipsBuyTotal,
   11:i64 moneyBuyTotal,
   12:i64 moneyLeft
   13:i64 safeWin,
   14:i64 safeCost,
   15:i64 createDate
}

struct StrangerGroupLogDetail {
    1:i64 alreadySecs,
    2:i64 leftSecs,
    3:list<StrangerGroupWinLoseLog> groupWinLoseLogList,
    4:string hours,
    5:i64 totalPlayCnt,
    6:string date
}

service StrangerEventService {
    string getServiceVersion()
    void gameEvent(1:string cmd, 2:string userId, 3:string data, 4:string tableId)
    map<string, string> joinGame(1:string groupId, 2:string userId)
    bool exitGame(1:string groupId, 2:string userId)
    map<string, string> sitDown(1:string groupId, 2:string userId, 3:i32 sitNum)
    bool standUp(1:string groupId, 2:string userId)
    TableInfo getTableInfo(1:string userId)
    StrangerResponse startGame(1:string groupId)
    StrangerResponse agreeUserBuyIn(1:string groupId, 2:string userId)
    map<string, string> getGroupUserStatus(1:string groupId)
    map<string, i64>  applyBuyIn(1:string groupId, 2:string userId, 3:i64 buyIn)
    map<string, i64> getBuyInInfo(1:string userId, 2:string groupId)
    map<string, string> userHasJoinGroup(1:string userId, 2:string groupId)
    map<string, i64> getGameTimes(1:string groupId)
    StrangerResponse changeSitDownType(1:string groupId, 2:string userId, 3:string type)
    StrangerResponse buySafe(1:string groupId, 2:string userId, 3:i32 buyIndex)
    list<StrangerGroup> getUserReferGroups(1:string userId)
    StrangerGroupLogDetail getStrangerGroupDetail(1:string groupId)
    StrangerResponse closeGame(1:string groupId)
    StrangerResponse leaderAddUserDepositMoney(1:string tableId, 2:string userId, 3:i32 buyInCnt, 4:i64 buyIn)
    map<string, i64> exChangeChips(1:string groupId, 2:string userId, 3:i64 buyInCnt)
    map<string, i64> getExchangeChipsInfo(1:string userId, 2:string groupId)
    map<string, string> checkUserTableGroup(1:string userId)
    string getGameRecord(1:string tableId, 2:string userId, 3:i32 cnt)
}