namespace java com.tiantian.stranger.thrift.task

const string version = '1.0.0'

service StrangerTaskService {
    string getServiceVersion()
     bool execTask(1:string taskStr)
}