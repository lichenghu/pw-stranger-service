-- KEYS[tableKeyPre, userRoomTableKeyPre, userId]
local keyLen = #KEYS
if keyLen ~= 3 then
    return {"0"}
end
local expireSeconds = 86400
local groupTableKeyPre = KEYS[1]
local groupUserRoomTableKeyPre = KEYS[2]
local userId = KEYS[3]

local groupUserRoomTableKey = groupUserRoomTableKeyPre .. userId
local tableId = redis.pcall("hget", groupUserRoomTableKey, "tableId")
if tableId then
    local tableKey = groupTableKeyPre .. tableId
    local sitNum = redis.pcall("hget", groupUserRoomTableKey, "sitNum")
    if sitNum then
        redis.pcall("hdel", tableKey, sitNum)
    end
end
--redis.pcall("hdel", userRoomTableKey, "sitNum") socket 端坐处理
redis.pcall("hset", groupUserRoomTableKey , "status", "standing")
redis.pcall("hset", groupUserRoomTableKey, "betStatus", "fold")
redis.pcall("hset", groupUserRoomTableKey, "operateCount", "")
redis.pcall("hset", groupUserRoomTableKey, "notOperateCount", "")
redis.pcall("expire", groupUserRoomTableKey, expireSeconds)
return {tableId}