local keyLen = #KEYS
if keyLen ~= 6 then
    return {"0", "0"}
end
local expireSeconds = 86400
local groupUserRoomTableKeyPre = KEYS[1]
local userId = KEYS[2]
local groupTableKeyPre = KEYS[3]
local maxNum = tonumber(KEYS[4])
local groupUserSitDownKeyPre = KEYS[5]
local selectSitNum = tonumber(KEYS[6])

local groupUserRoomTableKey = groupUserRoomTableKeyPre .. userId

local tableId = redis.pcall("hget", groupUserRoomTableKey, "tableId")
if not tableId then
    return {"0", "0"}
end

local groupUserSitDownKey = groupUserSitDownKeyPre .. tableId

local groupTableKey = groupTableKeyPre .. tableId
local hAll = redis.pcall("hgetAll", groupTableKey)
if hAll then
    for i, v in pairs(hAll) do
        -- 获取userId
        if i % 2 == 0 then
            if v == userId then
                -- 玩家还在游戏中不能选择座位 必须站起
                return {"-2", "0"}
            end
        end
    end
end

-- 判断人数
local userNums = #hAll / 2
if userNums >= maxNum then
    return {"-1", "0"} -- 座位已经满了
end


-- 获取座位
local sitNum = 0
-- 遍历key值即座位号
if hAll then
    for i = 1, maxNum do
        local find = true
        for a, v in pairs(hAll) do
            if a % 2 == 1 then
                if i == tonumber(v) then
                    find = false
                    break
                end
            end
        end
        if find and selectSitNum == i  then  -- 找到空位并且空位子和玩家选择的一样
           sitNum = i
           break
        end
    end
end

if sitNum == 0 then  -- 位子上已经坐下别人
    return {"-3", "0"}
end

redis.pcall("hset", groupTableKey, tostring(sitNum), userId)
redis.pcall("expire", groupTableKey, expireSeconds)

redis.pcall("hset", groupUserRoomTableKey, "sitNum", tostring(sitNum))
-- 设置状态为准备
redis.pcall("hset", groupUserRoomTableKey, "status", "preparing")
redis.pcall("hset", groupUserRoomTableKey, "betStatus", "")
redis.pcall("hset", groupUserRoomTableKey, "operateCount", "")
redis.pcall("hset", groupUserRoomTableKey, "notOperateCount", "")
redis.pcall("expire", groupUserRoomTableKey, expireSeconds)

redis.pcall("hset", groupUserSitDownKey, userId, "")
redis.pcall("expire", groupUserSitDownKey, expireSeconds)

return {tableId, tostring(sitNum)}