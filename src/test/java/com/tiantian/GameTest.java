package com.tiantian;

import akka.routing.Routee;
import akka.routing.RoutingLogic;
import com.alibaba.fastjson.JSON;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.stranger.akka.ClusterClientManager;
import com.tiantian.stranger.akka.user.TableUserStatusEvent;
import com.tiantian.stranger.data.mongodb.MGDatabase;
import com.tiantian.stranger.settings.StrangerEventConfig;
import com.tiantian.stranger.thrift.event.StrangerResponse;
import com.tiantian.stranger.utils.StrangerEventUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.msgpack.MessagePack;
import scala.collection.immutable.IndexedSeq;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class GameTest {
    private static final String GROUPS_TABLE = "stranger_groups";
    @Test
    public void testJoin() {
        String groupId = "577368aad4c6e8a7b762d685";
        String userId = "1sss";
        Map<String, String> map = StrangerEventUtils.getInstance().joinGame(groupId, userId);
        System.out.println(JSON.toJSONString(map));
        String userId2 = "2sss";
        Map<String, String> map2 = StrangerEventUtils.getInstance().joinGame(groupId, userId2);
        System.out.println(JSON.toJSONString(map2));
    }

    @Test
    public void testApplyBuyIn() {
        String groupId = "577368aad4c6e8a7b762d685";
        String userId = "1sss";
//        StrangerResponse response =  StrangerEventUtils.getInstance().applicationBuyIn(groupId, userId, 1);
//        System.out.println(JSON.toJSONString(response));
    }

    @Test
    public void testAgreeUserBuyIn() {
        String groupId = "577368aad4c6e8a7b762d685";
        String userId = "1sss";
        StrangerResponse response =  StrangerEventUtils.getInstance().agreeUserBuyIn(groupId, userId);
        System.out.println(JSON.toJSONString(response));
    }

    @Test
    public void testApplySafeBuyIn() {
        String groupId = "577368aad4c6e8a7b762d685";
        String userId = "1sss";
//        StrangerResponse response =  StrangerEventUtils.getInstance().applicationSafeBuyIn(groupId, userId, 1);
//        System.out.println(JSON.toJSONString(response));
    }

    @Test
    public void testAgreeUserSafeBuyIn() {
        String groupId = "577368aad4c6e8a7b762d685";
        String userId = "1sss";

    }

    @Test
    public void testStartGame() {
        String groupId = "577368aad4c6e8a7b762d685";
        StrangerResponse response = StrangerEventUtils.getInstance().startGame(groupId);
        System.out.println(JSON.toJSONString(response));
    }

    @Test
    public void testSitDown() {
        String groupId = "577368aad4c6e8a7b762d685";
//        String userId = "1sss";
        String userId2 = "1sss";
//        Map<String, String> map =  StrangerEventUtils.getInstance().sitDown(groupId, userId, 3);
//        System.out.println(JSON.toJSONString(map));
        Map<String, String> map2 = StrangerEventUtils.getInstance().sitDown(groupId, userId2, 4);
        System.out.println(JSON.toJSONString(map2));
    }

    @Test
    public void testStandUp() {
        String groupId = "577368aad4c6e8a7b762d685";
        String userId = "2sss";
        boolean ret = StrangerEventUtils.getInstance().standUp(groupId, userId);
        System.out.println(JSON.toJSONString(ret));
    }

    @Test
    public void testExit() {
        String groupId = "577368aad4c6e8a7b762d685";
        String userId = "2sss";
        boolean ret = StrangerEventUtils.getInstance().exitGame(groupId, userId);
        System.out.println(JSON.toJSONString(ret));
    }
    @Test
    public void testAdd() {

    }
    @Test
    public void testFriendCore() throws InterruptedException {
        MGDatabase.getInstance().init();
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId("5790b10f0cf27acf8a823dc0"));
        updateCondition.put("startDate",  new BasicDBObject("$gt", System.currentTimeMillis()));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("startDate", System.currentTimeMillis());
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        System.out.println((result.getModifiedCount() > 0) + "------");
    }

    @Test
    public void testSend() {
        ClusterClientManager.init((StrangerEventConfig.getInstance().getPort() + 5) + "");
        for(int i = 0; i< 1000; i++) {
            ClusterClientManager.send(new TableUserStatusEvent(123 + ""));
        }
    }
    @Test
    public void testPack() {

        MessagePack messagePack = new MessagePack();
//        long begin = System.currentTimeMillis();
//        System.out.println(begin);
        for (int a = 0; a < 1; a++) {
           new Thread(() -> {
               long begin = System.currentTimeMillis();
               TableUserStatusEvent tableUserStatusEvent = new TableUserStatusEvent("1");
                for (int i = 0; i < 15000; i++) {
                      //  TableUserStatusEvent tableUserStatusEvent = new TableUserStatusEvent("1");
                        try {
                            byte[] bytes = messagePack.write(tableUserStatusEvent);
                            messagePack.read(bytes, TableUserStatusEvent.class);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
                System.out.println(System.currentTimeMillis() - begin + ":" + System.currentTimeMillis());
           }).start();
        }
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
